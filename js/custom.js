    jQuery(".search-btn").click(function(){
        jQuery(".navbar-right").toggleClass("shower-search");
    });

// ===== Scroll to Top ==== 
jQuery('#return-to-top').click(function() {      // When arrow is clicked
    jQuery('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

jQuery(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 500) {
        jQuery(".top-scroll").addClass("show-button-bottom-to-top");
    } else {
        jQuery(".top-scroll").removeClass("show-button-bottom-to-top");
    }
});
jQuery(document).ready(function(){
jQuery('.carousel').carousel();
});

jQuery(document).ready(function() {

  jQuery(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    jQuery(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    jQuery(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    jQuery(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});